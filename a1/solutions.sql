-- A.
SELECT * FROM artists WHERE name LIKE '%d%';

-- B. 
SELECT * FROM songs WHERE length < 230;

-- C.
SELECT albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- D.
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE album_title LIKE "%a%"

-- E.
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- F.
SELECT * FROM albums
    JOIN songs ON songs.album_id = albums.id
    ORDER BY album_title DESC, song_name ASC;

SELECT * FROM songs
    JOIN albums ON albums.id = songs.album_id
    ORDER BY album_title DESC, song_name ASC;
    
    
    
    
    