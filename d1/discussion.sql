-- [SECTION] Add new records

-- Add Artists
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Beiber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add albums
INSERT INTO albums (album_title, date_released, artist_id) 
    VALUES (
        "Fearless",
        "2008-01-01",
        3
    );
INSERT INTO albums (album_title, date_released, artist_id) 
    VALUES (
        "Red",
        "2012-01-01",
        3
    );
INSERT INTO albums (album_title, date_released, artist_id) 
    VALUES (
        "A Start Is Born",
        "2018-01-01",
        4
    );
INSERT INTO albums (album_title, date_released, artist_id) 
    VALUES (
        "Born This Way",
        "2011-01-01",
        4
    );
INSERT INTO albums (album_title, date_released, artist_id) 
    VALUES (
        "Purpose",
        "2015-01-01",
        5
    );
INSERT INTO albums (album_title, date_released, artist_id) 
    VALUES (
        "Believe",
        "2012-01-01",
        5
    );
INSERT INTO albums (album_title, date_released, artist_id) 
    VALUES (
        "Dangerous Woman",
        "2016-01-01",
        6
    );

-- Add songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Fearless",
	246,
	"Pop rock",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Love Story",
	213,
	"Country pop",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"State of Grace",
	253,
	"Rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Red",
	204,
	"Country",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Black Eyes",
	151,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Born This Way",
	252,
	"Electropop",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Sorry",
	152,
	"Dancehall-op",
	7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Into You",
	242,
	"EDM",
	9
);

-- [SECTION] Advanced Selects

-- Exclude records
SELECT * FROM songs WHERE id != 5;

-- Greater than (or equal to)
SELECT * FROM songs WHERE id >= 4;

-- Less than (or equal to)
SELECT * FROM songs WHERE id <= 7;

-- Get specific songs via different search conditions (OR)
SELECT * FROM songs WHERE id = 1 OR song_name = "Red" OR length < 200;

-- Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 5, 6);

-- Find partial matches
-- Start search from end of string
SELECT * FROM songs WHERE song_name LIKE "%ss";
-- Start search from beginning of string
SELECT * FROM songs WHERE song_name LIKE "b%";
-- Search entire string
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- Upper/Lower
SELECT UPPER(song_name) FROM songs WHERE song_name = UPPER("red");
SELECT LOWER(song_name) FROM songs WHERE song_name = LOWER("RED");

-- Sort records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Limit returned records
SELECT * FROM songs ORDER BY song_name DESC LIMIT 3;

-- Getting distinct records (show all unique values)
SELECT DISTINCT genre FROM songs;

-- Count
SELECT COUNT(*) FROM songs WHERE genre = "Rock";

-- [SECTION] Table Joins

-- Combine artists and albums table (INNER JOIN)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

-- Left join (show all artists regardless of album status)
SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;

-- Right join (show all artists regardless of album status)
SELECT * FROM albums
	RIGHT JOIN artists ON albums.artist_id = artists.id;


-- Mini Activity:
-- In a single SELECT command, show all artists with their corresponding albums and songs
SELECT artists.name, albums.album_title, songs.song_name FROM artists
    LEFT JOIN albums ON artists.id = albums.artist_id;
    LEFT JOIN songs ON albums.id = songs.album_id;
    